//
//  doc2doc_preViewController.h
//  doc2doc_pre
//
//  Created by ManGoes Mobile on 15/4/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreGraphics/CoreGraphics.h>
#import "Cases.h"
#define CASE_PANE_MARGIN 10.0
#import "doc2doc_preAppDelegate.h"


@interface doc2doc_preViewController : UIViewController<UITableViewDelegate,UITableViewDataSource>
{
    NSMutableArray *cases;
    NSIndexPath *cellToBeDeleted;
    IBOutlet UITableView *caseList;
    IBOutlet UIScrollView *casePane;
    float case_pane_offset;
    BOOL gestureAction;
    
}
- (void) addSwipeViewTo:(UITableViewCell*)cell direction:(UISwipeGestureRecognizerDirection)direction;
-(IBAction)followButtonPressed:(id)sender;
-(IBAction)addItem:(id)sender;
-(void)addViewToScrollView;

@end
