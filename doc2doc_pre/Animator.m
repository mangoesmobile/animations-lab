//
//  Animator.m
//  doc2doc_pre
//
//  Created by ManGoes Mobile on 19/4/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import "Animator.h"

@implementation Animator

- (id)init
{
    self = [super init];
    if (self) {
        durations= [NSArray arrayWithObjects:@"0.28",@"0.26", @"0.24", @"0.22", @"0.2",nil];
        optionChecker=0;
        shouldBounce=NO;
    }
    
    return self;
}
//calculates the radian angle
-(float)calculateRadianForDegree:(float)angle
{
    float radian=(angle*M_PI)/180.0;
    return radian;
}
//
-(void)makeTranslationUsingAffineTransformForView:(UIView*)view ForAngle:(float)angle WithDuration:(float)duration
{
    float radAngle=[self calculateRadianForDegree:angle];
    float x=center_x+(R+10)*cosf(radAngle);
    float y=center_y-(R+10)*sinf(radAngle);
    float x2=center_x+(R+10)*cosf(radAngle);
    float y2=center_y-(R+10)*sinf(radAngle);

    [UIView animateWithDuration:0.3/2.5 animations:^{
        view.center=CGPointMake(x,y);
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:0.3/3 animations:^{
            view.center=CGPointMake(x2,y2);
        } completion:^(BOOL finished) {
        }];
    }];

}
//creates the translation block
-(CAAnimation*)makeTranslationBlockForView:(UIView*)view ForAngle:(float)angle WithDuration:(float)duration
{
    //
    CAKeyframeAnimation *moveAnimation = [CAKeyframeAnimation animationWithKeyPath:@"position"];
    CGMutablePathRef thePath = CGPathCreateMutable();
    //
    float radAngle=[self calculateRadianForDegree:angle];
    float x=center_x+(R+10)*cosf(radAngle);
    float y=center_y-(R+10)*sinf(radAngle);
    //
    float startingX=center_x;
    float startingY=center_y;
    float endingX=x;
    float endingY=y;
    
    //From this point
    CGPathMoveToPoint(thePath, NULL, startingX, startingY);
    //to this point
    CGPathAddLineToPoint(thePath, nil,endingX,endingY);
    //
    moveAnimation.path = thePath;
    moveAnimation.duration =duration;
    moveAnimation.delegate=self;
    [moveAnimation setValue:@"translation" forKey:@"translation"];
    moveAnimation.removedOnCompletion=NO;
    moveAnimation.fillMode=kCAFillModeForwards;
    return moveAnimation;

}
//creates the rotation block
-(CAAnimation*)makeRotationBlockForView:(UIView*)view ForAngle:(float)angle WithDuration:(float)duration
{
    float rad=[self calculateRadianForDegree:angle];
    float prevRad=[(NSNumber *)[view valueForKeyPath:@"layer.transform.rotation.z"] floatValue];
    //
    NSLog(@"Prev Rad:%f Rad:%f",prevRad,angle );
    //
    CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"transform.rotation"];
    animation.fromValue = [NSNumber numberWithFloat: prevRad ];
    animation.toValue =[NSNumber numberWithFloat: rad ];
    animation.removedOnCompletion=YES;
    animation.delegate=self;
    animation.autoreverses=NO;
    animation.speed=2;
    [animation setDuration:duration];
    return animation;
}

//Using basic animation translation
-(void)makeBasicTranslationOfView:(UIView*)view ForAngle:(float)angle WithDuration:(float)duration
{
    float radAngle=[self calculateRadianForDegree:angle];
    float x=center_x+(R+10)*cosf(radAngle);
    float y=center_y-(R+10)*sinf(radAngle);
    //
    intermediateView=view;
    lastPoint=CGPointMake(x,y);
        
    CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"position"];
    animation.fromValue = [NSValue valueWithCGPoint:CGPointMake(center_x, center_y)];
    animation.toValue = [NSValue valueWithCGPoint:CGPointMake(x, y)];
    animation.removedOnCompletion=NO;
    animation.fillMode=kCAFillModeBackwards;
    animation.autoreverses=NO;
    [animation setDuration:duration];
    [view.layer addAnimation:animation forKey:@"position" ]; 
}
//
-(void)plusToCross:(BOOL)flag Transform:(UIView*)view
{
    float  angle =flag?-45.0:90;
    [view.layer addAnimation:[self makeRotationBlockForView:view ForAngle:angle WithDuration:0.1] forKey:@"plus_rotation"];
}
//
//fan out completed, now make the bouncing effect 
- (void)fanOutCompleted
{
    //
    float angleInc=90.0/(([opt count]-2)+1);
    float angle=0.0;
    int i=0;

    for (UIView* view in opt) {
        view.center=view.layer.position;
        //i=0 indicates its the first one,so we don't need to add angleInc to angle.
        if(i!=0)
        {
            angle+=angleInc;
        }
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:[[durations objectAtIndex:i]floatValue]];
        //needed translation
        float x=center_x+(R*cosf([self calculateRadianForDegree:angle]));
        float y=center_y-(R*sinf([self calculateRadianForDegree:angle]));
        view.center=CGPointMake(x,y);
        [UIView commitAnimations];

        /*
         CAAnimationGroup *theGroup = [CAAnimationGroup animation];
         theGroup.delegate = self;
         theGroup.animations=[NSArray arrayWithObjects:translator,rotator, nil];
         [view.layer addAnimation:theGroup forKey:@"fan_out_animation"];
         */
        i++;
        
    }

        
}

//
-(void)animationDidStop:(CAAnimation *)anim finished:(BOOL)flag
{
   // CAKeyframeAnimation *keyframeAnimation = (CAKeyframeAnimation*)anim;
	//[intermediateView.layer setValue:[NSNumber numberWithInt:160] forKeyPath:keyframeAnimation.keyPath];
	//[intermediateView.layer removeAllAnimations];
    NSString *value=[anim valueForKey:@"translation"];
    if([value isEqualToString:@"translation"])
    {
        optionChecker++;
        if(optionChecker==numberOfOptions)
        {   
           // [self fanOutCompleted];
        }
        
    }


}

-(void)fanOutOptions:(NSMutableArray *)options FromAddButton:(UIButton *)button
{
    //save the button center
    center_x=button.center.x;
    center_y=button.center.y;
    //
    numberOfOptions=[options count];
    opt=[[NSMutableArray alloc] initWithArray:options];
    //
    [self plusToCross:YES Transform:button];
    //
    NSLog(@"button x=%f y=%f",center_x,center_y);
    //
    float angleInc=90.0/(([options count]-2)+1);
    float angle=0.0;
    int i=0;
    //
    for (UIView* view in options) {
        //init with -180 rotated state.
        view.layer.transform = CATransform3DMakeRotation([self calculateRadianForDegree:-180.0], 0, 0.0, 1.0);
        //i=0 indicates its the first one,so we don't need to add angleInc to angle.
        if(i!=0)
        {
            angle+=angleInc;
        }
        [self makeTranslationUsingAffineTransformForView:view ForAngle:angle WithDuration:[[durations objectAtIndex:i]floatValue]];
        /*
        CAAnimation* translator=[self makeTranslationBlockForView:view ForAngle:angle WithDuration:[[durations objectAtIndex:i]floatValue]];
        CAAnimation* rotator=[self makeRotationBlockForView:view ForAngle:360.0 WithDuration:0.4];
        [view.layer addAnimation:translator forKey:@"translation"];
        [view.layer addAnimation:rotator forKey:@"rotation"];
         */
        /*
        CAAnimationGroup *theGroup = [CAAnimationGroup animation];
        theGroup.delegate = self;
        theGroup.animations=[NSArray arrayWithObjects:translator,rotator, nil];
        [view.layer addAnimation:theGroup forKey:@"fan_out_animation"];
         */
        i++;

    }
}
-(void)fanInOptions:(NSMutableArray *)options FromCrossButton:(UIView *)button
{
    //save the button center
    center_x=button.center.x;
    center_y=button.center.y;
    
    [self plusToCross:NO Transform:button];


}

@end
