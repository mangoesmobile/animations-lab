//
//  CarouselController.m
//  doc2doc_pre
//
//  Created by ManGoes Mobile on 29/4/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import "CarouselController.h"

@implementation CarouselController
@synthesize carousel,items,wrap;
- (void)setData
{
    //set up data
    //your carousel should always be driven by an array of
    //data of some kind - don't store data in your item views
    //or the recycling mechanism will destroy your data once
    //your item views move off-screen
    self.items = [[NSMutableArray alloc] init];
    for (int i = 0; i < NUMBER_OF_ITEMS; i++)
    {
        NSString *text=[NSString stringWithFormat:@"%d",i+1];
        [items addObject:text];
    }
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(void)animateCarouselExistence
{
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.4];
    carousel.alpha=0.7;
    [UIView commitAnimations];
    
}
-(void)animateCarouselDissapear
{
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.2];
    
    carousel.alpha=0.0;

    [UIView commitAnimations];

}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    //Get all the touches.
    NSSet *allTouches = [event allTouches];
    
    //Number of touches on the screen
    switch ([allTouches count])
    {
        case 1:
        {
            //Get the first touch.
            UITouch *touch = [[allTouches allObjects] objectAtIndex:0];
            CGPoint loc=[touch locationInView:self.view];
            if(loc.x>=140 && loc.x <=184 && loc.y>=417 && loc.y<=460)
            {

                switch([touch tapCount])
                {
                    case 1://Single tap
                        NSLog(@"Inside touches began\n");
                        [self animateCarouselExistence];
                        carouselVisible=YES;
                        break;
                    case 2://Double tap.
                        if(carouselVisible)
                        {
                            carouselVisible=NO;
                            [self animateCarouselDissapear];
                            
                        }
                        break;
                }
            }
        } 
            break;
    }

}
- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}


#pragma mark -Carousel Methods
- (NSUInteger)numberOfItemsInCarousel:(iCarousel *)carousel
{
    //return the total number of items in the carousel
    return [items count];
}

- (CGFloat)carouselItemWidth:(iCarousel *)carousel
{
    //usually this should be slightly wider than the item views
    return ITEM_SPACING;
}


- (UIView *)carousel:(iCarousel *)carousel viewForItemAtIndex:(NSUInteger)index reusingView:(UIView *)view
{
    UIImageView *label = nil;
    
    //create new view if no view is available for recycling
    if (view == nil)
    {
        view = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"page.png"]];
        view.layer.doubleSided = NO; //prevent back side of view from showing
        label = [[UIImageView alloc]initWithFrame:view.bounds];
        label.contentMode=UIViewContentModeCenter;
        label.backgroundColor = [UIColor clearColor];
        [view addSubview:label];
    }
    else
    {
        label = [[view subviews] lastObject];
    }
    
    //set item label
    //remember to always set any properties of your carousel item
    //views outside of the `if (view == nil) {...}` check otherwise
    //you'll get weird issues with carousel item content appearing
    //in the wrong place in the carousel
    label.image=[UIImage imageNamed:[NSString stringWithFormat:@"%@.png",(NSString*)[items objectAtIndex:index]]];
    return view;
}


#pragma mark - View lifecycle

/*
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView
{
}
*/


- (void)viewDidLoad
{
    [super viewDidLoad];
    //
    [self setData];
    //
    carouselVisible=NO;
    carousel.alpha=0.0;
    //configure carousel
    carousel.type=iCarouselTypeRotary;
    carousel.dataSource=self;
    carousel.delegate=self;
    //
    
    
}

- (void)viewDidUnload
{

    containerView = nil;
    backView = nil;
    backView = nil;
    frontView = nil;
    leftView = nil;
    rightView = nil;
    dialButton = nil;
    views=nil;
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
