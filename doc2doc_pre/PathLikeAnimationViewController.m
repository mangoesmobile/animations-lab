//
//  PathLikeAnimationViewController.m
//  doc2doc_pre
//
//  Created by ManGoes Mobile on 20/4/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import "PathLikeAnimationViewController.h"

@implementation PathLikeAnimationViewController
@synthesize container,label;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
//checks if childView is in self.view
-(BOOL)parentViewContains:(UIView*)childView
{
    BOOL retVal=NO;
    for (UIView *view in self.view.subviews) {
        if(view.tag==childView.tag)
        {
            retVal=YES;
            break;
        }
    }
    return retVal;
}


//creates a circular UIView
-(void)setRoundedView:(UIView *)roundedView toDiameter:(float)newSize;
{
    CGPoint saveCenter = roundedView.center;
    CGRect newFrame = CGRectMake(roundedView.frame.origin.x, roundedView.frame.origin.y, newSize, newSize);
    roundedView.frame = newFrame;
    roundedView.layer.cornerRadius = newSize / 2.0;
    roundedView.center = saveCenter;
}
//
- (void)createRoundCompassInBetween:(CGPoint)touch1point And:(CGPoint)touch2point {
    //CGFloat distance = sqrt((xDist * xDist) + (yDist * yDist));
    circularContainer.center=CGPointMake((touch2point.x+touch1point.x)/2,(touch2point.y+touch1point.y)/2);
    //
    _menu.startPoint=circularContainer.center;
    [_menu _update];
    //add the circular container
    if(![self parentViewContains:circularContainer])
        [self.view addSubview:circularContainer];
    else
    {
        [circularContainer removeFromSuperview];
        [self.view addSubview:circularContainer];
    }
    //add the menu
    if(![self parentViewContains:_menu])
    {
        [self.view addSubview:_menu];
        menuButtonShowed=YES;
        _menu.alpha=1.00;
    }
    else
    {
        [_menu removeFromSuperview];
        [self.view addSubview:_menu];
        menuButtonShowed=YES;
        _menu.alpha=1.00;
    }
    //always keep the menu infront of the circle
    [self.view bringSubviewToFront:_menu];
    

}
//detect when the two fingers have been placed
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    NSSet *touch=[event allTouches];
    NSArray *touch_array=[touch allObjects];
    //2 fingers has been touched
    if([touch_array count]==2)
    {
        NSLog(@"%d fingers touched!",[touch_array count]);
        UITouch *touch1=[touch_array objectAtIndex:0];
        UITouch *touch2=[touch_array objectAtIndex:1];
        CGPoint touch1point=[touch1 locationInView:self.view];
        CGPoint touch2point=[touch2 locationInView:self.view];
        CGFloat xDist = (touch2point.x - touch1point.x);
        CGFloat yDist = (touch2point.y - touch1point.y);
        CGFloat distance=sqrtf(xDist*xDist + yDist*yDist);
        [self createRoundCompassInBetween:touch1point And:touch2point];
    }
}

-(void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    NSSet *touch=[event allTouches];
    NSArray *touch_array=[touch allObjects];
    //2 fingers has been touched
    if([touch_array count]==2)
    {
        if(menuButtonShowed)
        {
            menuButtonShowed=NO;
            _menu.alpha=0.0;
            [_menu removeFromSuperview];
            [circularContainer removeFromSuperview];
            
        }

    }

}
//init the gesture recognizers for the view
- (void)handlePinchGesture:(UIGestureRecognizer *)sender
{
    if([sender state]==UIGestureRecognizerStateEnded)
    {
        if(menuButtonShowed)
        {
            menuButtonShowed=NO;
            _menu.alpha=0.0;
            [_menu removeFromSuperview];
            [circularContainer removeFromSuperview];
            
        }
    }
    float scale=((UIPinchGestureRecognizer*)sender).scale;
    NSLog(@"scale factor:%f",scale);
    circularContainer.transform=CGAffineTransformMakeScale(scale*2, scale*2);
    int scaleFactor=scale;
    if(scaleFactor==2)
    {
        if(!_menu.isExpanding)
        [_menu setExpanding:YES];
    }
    else if(scale<2.0)
    {
        if(_menu.isExpanding)
        {
            [_menu setExpanding:NO];
        }
    }
            
}
-(void)initGestureRecognizers
{
     pinchGesture= [[UIPinchGestureRecognizer alloc]
                                              initWithTarget:self action:@selector(handlePinchGesture:)];
    [self.view addGestureRecognizer:pinchGesture];
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}
-(void)createMenu
{
    NSMutableArray *menus=[[NSMutableArray alloc] init];
    for(int i=1;i<=4;i++)
    {
        UIImage *storyMenuItemImage = [UIImage imageNamed:[NSString stringWithFormat:@"%d.png",i]];
        UIImage *storyMenuItemImagePressed = [UIImage imageNamed:[NSString stringWithFormat:@"%d.png",i]];
        AwesomeMenuItem *menuItem=[[AwesomeMenuItem alloc] initWithImage:storyMenuItemImage highlightedImage:storyMenuItemImagePressed];
        [menus addObject:menuItem];

    }
        
        //the central menu button
    AwesomeMenuItem *menuButton= [[AwesomeMenuItem alloc] initWithImage:[UIImage imageNamed:@"praxis.png"]
                                                       highlightedImage:nil ContentImage: nil highlightedContentImage:nil ];
    AwesomeMenu *menu = [[AwesomeMenu alloc] initWithFrame:self.container.bounds menuButton:menuButton andMenus:menus];
    //customize the menu
    menu.farRadius=100.0f;
    menu.nearRadius=80.0f;
    menu.endRadius=90.0f;
    [menu _update];
    //
    [menu autoExpandWithTouch:NO];
    _menu=menu;
    _menu.delegate=self;
    _menu.alpha=0.0f;
    _menu.tag=MENU_TAG;

    [self.view addSubview:_menu];
        

}
#pragma mark-awesome menu delegates
- (void)AwesomeMenu:(AwesomeMenu *)menu didSelectIndex:(NSInteger)idx
{
    NSLog(@"Select the index : %d",idx);
}



#pragma mark - View lifecycle

/*
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView
{
}
*/

/*
// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
 */
- (void)viewDidLoad
{
    [super viewDidLoad];
    buttonTapped=NO;
    animator=[[Animator alloc] init];
    options=[[NSMutableArray alloc] init];
    menuButtonShowed=NO;
    //
    circularContainer=[[UIView alloc] init];
    circularContainer.backgroundColor=[UIColor blackColor];
    circularContainer.alpha=0.3;
    [self setRoundedView:circularContainer toDiameter:CIRCLE_INITIAL_DIAMETER];
    circularContainer.tag=CONTAINER_TAG;
    //
    [self createMenu];
    [self initGestureRecognizers];
    
}

- (void)viewDidUnload
{
    addButton = nil;
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (IBAction)addButtonPressed:(id)sender 
{
    if(_menu.isExpanding)
    {
        [_menu setExpanding:NO];
    }
    else
    {
        [_menu setExpanding:YES];
    }
}
@end
