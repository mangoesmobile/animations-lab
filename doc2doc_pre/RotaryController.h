//
//  RotaryController.h
//  doc2doc_pre
//
//  Created by ManGoes Mobile on 30/4/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "iCarousel.h"
#define NUMBER_OF_ITEMS 4
#define ITEM_SPACING 100.0f

@interface RotaryController : UIViewController<iCarouselDataSource, iCarouselDelegate>
{
    BOOL carouselVisible;
    CGPoint dialButtonPos;
}
@property (nonatomic, retain) IBOutlet iCarousel *carousel;
@property (nonatomic, retain) NSMutableArray *items;
@property (nonatomic, retain) UIButton *dialButton;



@end
