//
//  main.m
//  doc2doc_pre
//
//  Created by ManGoes Mobile on 15/4/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "doc2doc_preAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([doc2doc_preAppDelegate class]));
    }
}
