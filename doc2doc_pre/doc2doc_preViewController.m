//
//  doc2doc_preViewController.m
//  doc2doc_pre
//
//  Created by ManGoes Mobile on 15/4/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import "doc2doc_preViewController.h"

@implementation doc2doc_preViewController

-(IBAction)addItem:(id)sender
{
    Cases *case1;
    case1=[[Cases alloc] init];
    case1.name=@"Your case";
    case1.date=@"00/00/0000";
    [cases addObject:case1];
    [caseList reloadData];
}
-(void)setUpTouchRecognizer
{
    // Setup a left swipe gesture recognizer
    UISwipeGestureRecognizer* leftSwipeGestureRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeLeft:)];
    leftSwipeGestureRecognizer.direction = UISwipeGestureRecognizerDirectionLeft;
    [caseList addGestureRecognizer:leftSwipeGestureRecognizer];
    //Setup a right swipe gesture recognizer
    // Setup a left swipe gesture recognizer
    UISwipeGestureRecognizer* rightSwipeGestureRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeRight:)];
    rightSwipeGestureRecognizer.direction = UISwipeGestureRecognizerDirectionRight;
    [caseList addGestureRecognizer:rightSwipeGestureRecognizer];

}
-(void)swipeLeft:(UISwipeGestureRecognizer*)recog
{
    CGPoint location = [recog locationInView:caseList];
    cellToBeDeleted = [caseList indexPathForRowAtPoint:location];
    UITableViewCell *swipedCell  = [caseList cellForRowAtIndexPath:cellToBeDeleted];
    UIView *bg=[[UIView alloc] initWithFrame:swipedCell.frame];
    bg.backgroundColor=[UIColor grayColor];
    swipedCell.backgroundView=bg;
    [self addSwipeViewTo:swipedCell direction:[recog direction]];
    
}
/*COME HERE*/

-(void)swipeRight:(UISwipeGestureRecognizer*)recog
{
    CGPoint location = [recog locationInView:caseList];
    cellToBeDeleted = [caseList indexPathForRowAtPoint:location];
    UITableViewCell *swipedCell  = [caseList cellForRowAtIndexPath:cellToBeDeleted];
    UIView *bg=[[UIView alloc] initWithFrame:swipedCell.frame];
    bg.backgroundColor=[UIColor greenColor];
    swipedCell.backgroundView=bg;
    [self addSwipeViewTo:swipedCell direction:[recog direction]];
    
}


- (void)animationDidStop:(NSString*)animationID finished:(BOOL)finished context:(void *)context 
{
    UITableViewCell *cell=[caseList cellForRowAtIndexPath:cellToBeDeleted];
    [cell removeFromSuperview];
    [cases removeObjectAtIndex:cellToBeDeleted.row];
    [caseList deleteRowsAtIndexPaths:[NSArray arrayWithObject:cellToBeDeleted] withRowAnimation:UITableViewRowAnimationMiddle];
}





- (void) addSwipeViewTo:(UITableViewCell*)cell direction:(UISwipeGestureRecognizerDirection)direction
{
    if(direction==UISwipeGestureRecognizerDirectionLeft)
    {
        CGRect cellFrame = cell.frame;
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:0.5];
        [UIView setAnimationDelegate:self];
        [UIView setAnimationDidStopSelector:@selector(animationDidStop:finished:context:)];
        cell.frame = CGRectMake(-cellFrame.size.width, cellFrame.origin.y, cellFrame.size.width, cellFrame.size.height);
        [UIView commitAnimations];
    }
    else
    {
        //THE FOLLOW BUTTON ACTION IS NOW INITIATED VIA GESTURE
        gestureAction=YES;
        //
        CGRect cellFrame = cell.frame;
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:0.5];
        [UIView setAnimationDelegate:self];
        [UIView setAnimationDidStopSelector:@selector(followButtonPressed:)];
        cell.frame = CGRectMake(cellFrame.size.width, cellFrame.origin.y, cellFrame.size.width, cellFrame.size.height);
        [UIView commitAnimations];

        
    }
}

-(void)addViewToScrollView
{
    //
    UIView *temp_view=[[UIView alloc] initWithFrame:CGRectMake(case_pane_offset+self.view.frame.size.width, 10, 67.5, 64)];
    //
    UILabel *label=[[UILabel alloc] initWithFrame:CGRectMake(0, 0, 67.5, 64)];
    
    label.textAlignment=UITextAlignmentCenter;
    [temp_view addSubview:label];
    //
    [casePane addSubview:temp_view];
    
    //
    label.text=[NSString stringWithFormat:@"%.0f",case_pane_offset];
    //
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.2];
    
    temp_view.frame = CGRectMake(case_pane_offset, temp_view.frame.origin.y, temp_view.frame.size.width, temp_view.frame.size.height);
    [UIView commitAnimations];
    //
    case_pane_offset+=temp_view.frame.size.width;
    
}
/*
//table view specific methods
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{

}

*/
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [cases count];
}

/*
 CREATES CELL FOR LIST BASED MENU
 */
- (UITableViewCell*)createTableCellForListMenu:(UITableView *)tableView indexPath:(NSIndexPath *)indexPath {
    
    static NSString *cellIdentifier=@"Cell";
    UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if(cell==nil) 
    {
        cell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier];
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
        
    }
    Cases *temp=[cases objectAtIndex:indexPath.row] ;
    cell.textLabel.text=temp.name;
    cell.textLabel.adjustsFontSizeToFitWidth=NO;
    cell.textLabel.numberOfLines=0;
    cell.textLabel.backgroundColor=[UIColor clearColor];
    //
    cell.detailTextLabel.text=temp.date;
    cell.detailTextLabel.backgroundColor=[UIColor clearColor];
    //
    UIButton *followButton=[UIButton buttonWithType:UIButtonTypeRoundedRect];
    followButton.frame=CGRectMake(0, 0,64,32);
    [followButton setTitle:@"Follow" forState:UIControlStateNormal];
    [followButton addTarget:self action: @selector(followButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    followButton.tag=indexPath.row;
    //
    cell.accessoryView=followButton;
    //
    UIView *bg=[[UIView alloc] initWithFrame:cell.frame];
    bg.backgroundColor=[UIColor orangeColor];
    cell.backgroundView=bg;

    
    return cell;
}

-(UITableViewCell* )tableView:(UITableView*)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    return [self createTableCellForListMenu:tableView indexPath:indexPath];
    
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Release any cached data, images, etc that aren't in use.
}
-(void)initData
{
    Cases *case1,*case2,*case3,*case4,*case5,*case6;
    //
    case1=[[Cases alloc] init];
    case1.name=@"Diarrhoea";
    case1.date=@"21/12/2012";
    //
    case2=[[Cases alloc] init];
    case2.name=@"Diptheria";
    case2.date=@"21/10/2012";
    //
    case3=[[Cases alloc] init];
    case3.name=@"Headache";
    case3.date=@"01/11/2012";
    //
    //
    case4=[[Cases alloc] init];
    case4.name=@"Diarrhoea";
    case4.date=@"21/12/2012";
    //
    case5=[[Cases alloc] init];
    case5.name=@"Diptheria";
    case5.date=@"21/10/2012";
    //
    case6=[[Cases alloc] init];
    case6.name=@"Headache";
    case6.date=@"01/11/2012";
    //

    cases=[[NSMutableArray alloc] initWithArray:[NSArray arrayWithObjects:case1,case2,case3,case4,case5,case6, nil]];

    
}
//
-(IBAction)followButtonPressed:(id)sender
{
    //THIS IS INITIATED BY GESTURE.
    if(gestureAction)
    {
        UITableViewCell *cell=[caseList cellForRowAtIndexPath:cellToBeDeleted];
        [cell removeFromSuperview];
        [cases removeObjectAtIndex:cellToBeDeleted.row];
        [caseList deleteRowsAtIndexPaths:[NSArray arrayWithObject:cellToBeDeleted] withRowAnimation:UITableViewRowAnimationMiddle];
        //SET TO DEFAULT.
        gestureAction=NO;
    }
    case_pane_offset+=CASE_PANE_MARGIN;
    //JUST CROSSED THE EDGE, ANIMATE TO THAT REGION.
    if(case_pane_offset>=casePane.contentOffset.x+casePane.frame.size.width)
    {
        //AT EDGE,SO NEEDS SOME MARGIN.
        case_pane_offset+=CASE_PANE_MARGIN;

        CGFloat prevY=casePane.contentOffset.y;
        CGFloat prevWidth=casePane.contentSize.width;
        casePane.contentSize=CGSizeMake(casePane.contentSize.width+casePane.frame.size.width,casePane.frame.size.height);
        [casePane setContentOffset:CGPointMake(prevWidth, prevY) animated:YES];
    }
    [self addViewToScrollView];

}



#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initData];
    [self setUpTouchRecognizer];
    gestureAction=NO;
    case_pane_offset=0;
    casePane.contentSize=CGSizeMake(self.view.frame.size.width, casePane.frame.size.height);
    //save it as the current view controller.
    ((doc2doc_preAppDelegate*)[UIApplication sharedApplication].delegate).current_controller=self;
    
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)viewDidUnload
{
    caseList = nil;
    casePane = nil;
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
	[super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
	[super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}

@end
