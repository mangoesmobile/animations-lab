//
//  doc2doc_preAppDelegate.h
//  doc2doc_pre
//
//  Created by ManGoes Mobile on 15/4/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface doc2doc_preAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property(strong,nonatomic) UIViewController *current_controller;

@end
