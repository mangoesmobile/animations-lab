//
//  CircularNavPaneController.h
//  doc2doc_pre
//
//  Created by ManGoes Mobile on 12/5/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#define DEFAULT_HELP @"1.Touch 2 fingers.\n 2.Pinch out until you see a red border around the rainbow pane.\n 3.Tap any icon to select that."
#define SELECTED_MENU_ID 23456

@interface CircularNavPaneController : UIViewController
{
    UIPinchGestureRecognizer *pinchGesture;
    UIImageView *circularContainer;
    UIImageView *deltaButton;
    //denotes whether the zoom level reached red mark is shown.
    BOOL markShown;
    //default container size.
    CGSize containerDefaultSize;
    NSArray *menus;
    //selected menu that's cloned and added to main view.
    UIButton * selectedMenu;
    //this view will hold the menus
    UIView *overlayView;
    //the point where currently selected page icon will be shown.
    CGPoint placeHolder;


}
//the help label.
@property (strong,nonatomic) IBOutlet UILabel *infoLabel;
//helps to identify when the user reaches predefined pinch(zoom) level.
@property (strong,nonatomic) IBOutlet UIView *boundManager;


@end
