//
//  Animator.h
//  doc2doc_pre
//
//  Created by ManGoes Mobile on 19/4/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <QuartzCore/QuartzCore.h>
#define R 100

@interface Animator : NSObject
{
    float center_x;
    float center_y;
    NSMutableArray *opt;
    //this view is kept intentionally to enable bouncing effect at end of fanout/in effect
    UIView *intermediateView;
    float intermediateAngle;
    CGPoint lastPoint;
    //
    NSArray *durations;
    int numberOfOptions;
    int optionChecker;
    //
    BOOL shouldBounce;
}
-(void)fanOutOptions:(NSMutableArray*)options FromAddButton:(UIButton*)button;
-(void)fanInOptions:(NSMutableArray*)options FromCrossButton:(UIView*)button;

@end
