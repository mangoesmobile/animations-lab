//
//  CarouselController.h
//  doc2doc_pre
//
//  Created by ManGoes Mobile on 29/4/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CarouselView.h"
#import "iCarousel.h"
#define SCALE_VALUE 16.0
#define NUMBER_OF_ITEMS 4
#define NUMBER_OF_VISIBLE_ITEMS 4;
#define ITEM_SPACING 100.0f
#define INCLUDE_PLACEHOLDERS YES

@interface CarouselController : UIViewController<iCarouselDataSource, iCarouselDelegate>
{
    
    IBOutlet UIView *containerView;
    //pointers connected to real views
    IBOutlet CarouselView *backView;
    IBOutlet CarouselView *frontView;
    IBOutlet CarouselView *leftView;
    IBOutlet CarouselView *rightView;
    IBOutlet UIButton *dialButton;
    //views placeholder
    NSMutableArray *views;
    //positions
    CGPoint pos1,pos2,pos3,pos4;
    BOOL carouselVisible;
    BOOL carouselStarted;//indicates that carousel animation has been started in one direction.
    int viewCounter;//how many views have completed the animation
    //point to compare to get swipe direction
    CGPoint pivot;
}
@property (nonatomic, retain) IBOutlet iCarousel *carousel;
@property (nonatomic, assign) BOOL wrap;
@property (nonatomic, retain) NSMutableArray *items;


@end
