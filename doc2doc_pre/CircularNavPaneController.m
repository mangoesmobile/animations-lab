//
//  CircularNavPaneController.m
//  doc2doc_pre
//
//  Created by ManGoes Mobile on 12/5/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import "CircularNavPaneController.h"

@implementation CircularNavPaneController
@synthesize infoLabel,boundManager;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}
#pragma mark-check methods
-(BOOL)containsSelectedMenu
{
    BOOL retVal=NO;
    for (UIView *view in self.view.subviews) {
        if(view.tag==SELECTED_MENU_ID)
        {
            retVal=YES;
            break;
        }
    }
    return retVal;
}
#pragma mark-animation methods
//grow and translate the selected menu(actually clone of it) to overlay view's center.
-(void)growAndTranslateView:(UIView*)view1 To:(UIView*)view2
{
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.4];
    [UIView setAnimationDelegate:self];
    view1.center=view2.center;
    view1.transform=CGAffineTransformMakeScale(1.5, 1.5);
    [UIView commitAnimations];
 
}
//as soon as growing and translation completes, the enlarged icon starts to rotate and translate to placeholder.
-(void)rotateAndTranslateView:(UIView*)view1 To:(CGPoint)point
{
    CAKeyframeAnimation *rotateAnimation = [CAKeyframeAnimation animationWithKeyPath:@"transform.rotation.z"];
    rotateAnimation.values = [NSArray arrayWithObjects:[NSNumber numberWithFloat:0.0f],[NSNumber numberWithFloat:M_PI*2],[NSNumber numberWithFloat:0.0f], nil];
    rotateAnimation.duration = 0.5f;
    rotateAnimation.keyTimes = [NSArray arrayWithObjects:
                                [NSNumber numberWithFloat:.0], 
                                [NSNumber numberWithFloat:.4],
                                [NSNumber numberWithFloat:.5], nil]; 
    rotateAnimation.autoreverses=NO;
    rotateAnimation.removedOnCompletion=NO;
    
    CAKeyframeAnimation *positionAnimation = [CAKeyframeAnimation animationWithKeyPath:@"position"];
    positionAnimation.duration = 0.5f;
    CGMutablePathRef path = CGPathCreateMutable();
    //starts at its position
    CGPathMoveToPoint(path, NULL, view1.center.x, view1.center.y);
    //move a bit far
    CGPathAddLineToPoint(path, NULL, view1.center.x+10, view1.center.y+10);
    //then start moving to destination
    CGPathAddLineToPoint(path, NULL, point.x, point.y); 
    positionAnimation.path = path;
    CGPathRelease(path);
    positionAnimation.removedOnCompletion=NO;
    positionAnimation.autoreverses=NO;
    
    //rotate animation has been skipped for the time being
    CAAnimationGroup *animationgroup = [CAAnimationGroup animation];
    animationgroup.animations = [NSArray arrayWithObjects:positionAnimation, nil];
    animationgroup.duration = 0.5f;
    animationgroup.autoreverses=NO;
    animationgroup.removedOnCompletion=NO;
    animationgroup.fillMode = kCAFillModeForwards;
    animationgroup.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseIn];    
    [selectedMenu.layer addAnimation:animationgroup forKey:@"animations"];
    
}

//perform shrinking and dissappearing of menu items as soon as the selected menu clone gets grown and translates to center of overlay view.
-(void)performOtherAnimationsAfterMenuClicked
{
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.4];
    //each menu item should be shrinked and invisible.
    for (UIButton *menu in menus) 
    {
        menu.transform=CGAffineTransformMakeScale(0.01, 0.01);
        menu.alpha=0;

    }
    //the other views should retain previous state.
    circularContainer.transform=CGAffineTransformIdentity;
    circularContainer.alpha=0;
    deltaButton.alpha=0;
    boundManager.alpha=0;
    [UIView commitAnimations];    
}
//grow animation of selected menu is completed.
-(void)animationDidStop:(CAAnimation *)anim finished:(BOOL)flag
{
    [self performOtherAnimationsAfterMenuClicked];
    //now rotate and translate it to placeholder.
    [self rotateAndTranslateView:selectedMenu To:placeHolder];
    
}


-(void)fadeInView:(UIView*)iView To:(float)value
{
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.4];
    iView.alpha=value;
    [UIView commitAnimations];
    
}

-(void)fadeInMenus
{
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.3];
    for (UIButton *menu in menus) 
    {
        menu.transform=CGAffineTransformIdentity;
        menu.alpha=1;
    }
    [UIView commitAnimations];

}

#pragma mark - view element manipulation
-(void)addMenusToOverlayView
{
    for (UIButton *menu in menus) 
    {
        [overlayView addSubview:menu];
    }
}
-(void)createTheMenus
{
    //default width and height 
    float sizeValue=40;
    //the factor which is half of size factor just to place the button rectangles in properplace
    float scaleFactor=sizeValue/2;
    //distance of buttons from center
    float radius=60;
    UIButton *menu1=[UIButton buttonWithType:UIButtonTypeCustom];
    menu1.userInteractionEnabled=YES;
    [menu1 setBackgroundImage:[UIImage imageNamed:@"1.png"] forState:UIControlStateNormal];
    [menu1 addTarget:self action: @selector(menuPressed:) forControlEvents:UIControlEventTouchUpInside];
    menu1.frame=CGRectMake(overlayView.frame.size.width/2+radius-scaleFactor, overlayView.frame.size.height/2-10,sizeValue,sizeValue);
    menu1.alpha=0;
    menu1.tag=1;
    //
    UIButton *menu2=[UIButton buttonWithType:UIButtonTypeCustom];
    menu2.userInteractionEnabled=YES;
    [menu2 setBackgroundImage:[UIImage imageNamed:@"2.png"] forState:UIControlStateNormal];    
    [menu2 addTarget:self action: @selector(menuPressed:) forControlEvents:UIControlEventTouchUpInside];
    menu2.frame=CGRectMake(overlayView.frame.size.width/2-radius-scaleFactor, overlayView.frame.size.height/2-12,sizeValue,sizeValue);
    menu2.alpha=0;
    menu2.tag=2;
    //
    UIButton *menu3=[UIButton buttonWithType:UIButtonTypeCustom];
    menu3.userInteractionEnabled=YES;
    [menu3 setBackgroundImage:[UIImage imageNamed:@"3.png"] forState:UIControlStateNormal];
    [menu3 addTarget:self action: @selector(menuPressed:) forControlEvents:UIControlEventTouchUpInside];
    menu3.frame=CGRectMake(overlayView.frame.size.width/2-scaleFactor, overlayView.frame.size.height/2+radius-scaleFactor,sizeValue,sizeValue);
    menu3.alpha=0;
    menu3.tag=3;
    //
    UIButton *menu4=[UIButton buttonWithType:UIButtonTypeCustom];
    menu4.userInteractionEnabled=YES;
    [menu4 setBackgroundImage:[UIImage imageNamed:@"4.png"] forState:UIControlStateNormal];
    [menu4 addTarget:self action: @selector(menuPressed:) forControlEvents:UIControlEventTouchUpInside];
    menu4.frame=CGRectMake(overlayView.frame.size.width/2-scaleFactor, overlayView.frame.size.height/2-radius-scaleFactor,sizeValue,sizeValue);
    menu4.alpha=0;
    menu4.tag=4;
    if([menus count]!=0)
        menus=nil;
    menus=[NSArray arrayWithObjects:menu1,menu2,menu3,menu4, nil];
    
}
-(void)markTheCircle
{
    boundManager.backgroundColor=[UIColor clearColor];
    boundManager.alpha=1;
    boundManager.layer.borderColor = [UIColor redColor].CGColor;
    boundManager.layer.borderWidth = 3.0f;

}
-(void)unmarkTheCircle
{
    boundManager.layer.borderWidth=0;
    boundManager.alpha=0.1;
    boundManager.backgroundColor=[UIColor grayColor];
}
- (void)createRoundCompassInBetween:(CGPoint)touch1point And:(CGPoint)touch2point {
    //CGFloat distance = sqrt((xDist * xDist) + (yDist * yDist));
    CGPoint center=CGPointMake((touch2point.x+touch1point.x)/2,(touch2point.y+touch1point.y)/2);
    //if the bound manager is previously bordered
    if(markShown)
        [self unmarkTheCircle];
    //set the bound manager's center to this point
    boundManager.center=center;
    circularContainer.frame=CGRectMake(center.x-containerDefaultSize.width/2, center.y-containerDefaultSize.height/2,containerDefaultSize.width,containerDefaultSize.height);
    deltaButton.center=circularContainer.center;
    overlayView.center=circularContainer.center;
    //fade the deltaButton and background container in
    [self fadeInView:deltaButton To:1];
    [self fadeInView:circularContainer To:1];
    [self fadeInView:boundManager To:0.1];
    //add menus to overlay view
    [self addMenusToOverlayView];

    
    
    
}



#pragma mark- touch initiation
//detect when the two fingers have been placed and then create the navigation pane.
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    NSSet *touch=[event allTouches];
    NSArray *touch_array=[touch allObjects];
    //2 fingers has been touched
    if([touch_array count]==2)
    {
        UITouch *touch1=[touch_array objectAtIndex:0];
        UITouch *touch2=[touch_array objectAtIndex:1];
        CGPoint touch1point=[touch1 locationInView:self.view];
        CGPoint touch2point=[touch2 locationInView:self.view];
        [self createRoundCompassInBetween:touch1point And:touch2point];
        
    }
}

-(void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    NSSet *touch=[event allTouches];
    NSArray *touch_array=[touch allObjects];
    //2 fingers has been touched
    if([touch_array count]==2)
    {
                
    }
    
}
//init the gesture recognizers for the view
- (void)handlePinchGesture:(UIGestureRecognizer *)sender
{
    UIPinchGestureRecognizer *gestureRecognizer=(UIPinchGestureRecognizer*)sender;
    float scale=gestureRecognizer.scale;
    //check upto a certain level
    if(circularContainer.frame.size.width<boundManager.frame.size.width-5)
    {
        if(circularContainer.frame.size.width>boundManager.frame.size.width-10)
        {
            //the desired width achieved mark the border as red
            [self markTheCircle];
            [self fadeInMenus];
            markShown=YES;
        }
        CGAffineTransform transform=CGAffineTransformScale(circularContainer.transform, scale,scale);
        circularContainer.transform=transform;
        gestureRecognizer.scale=1;
    }
        
    
}
//init a gesture recognizer for the main view.
-(void)initGestureRecognizers
{
    pinchGesture= [[UIPinchGestureRecognizer alloc]
                   initWithTarget:self action:@selector(handlePinchGesture:)];
    [self.view addGestureRecognizer:pinchGesture];
}
#pragma mark-action methods
//when a menu is selected.
-(IBAction)menuPressed:(id)sender
{
    UIButton *button=(UIButton*)sender;
    //clone the menu and add it to main view,so that we can move it to placeholder.
    UIButton *cloneMenu=[UIButton buttonWithType:UIButtonTypeCustom];
    NSString *imgName=[NSString stringWithFormat:@"%d.png",button.tag];
    [cloneMenu setBackgroundImage:[UIImage imageNamed:imgName] forState:UIControlStateNormal];
    //the X co-ordinate of overlay view.
    float parentX=overlayView.frame.origin.x;
    //the X co-ordinate of seleceted view respective to overlay view.
    float childX=button.frame.origin.x;
    //the Y co-ordinate of overlay view.
    float parentY=overlayView.frame.origin.y;
    //the Y co-ordinate of seleceted view respective to overlay view.
    float childY=button.frame.origin.y;
    
    cloneMenu.frame=CGRectMake(parentX+childX, parentY+childY, button.frame.size.width, button.frame.size.height);
    //make sure its not in main view
    [selectedMenu removeFromSuperview];
    selectedMenu=nil;
    //add cloned menu to selected menu
    selectedMenu=cloneMenu;
    //add the selected menu
    [self.view addSubview:selectedMenu];
    [self growAndTranslateView:selectedMenu To:overlayView];
    //change the background of main view based on selection
    switch (button.tag) {
        case 1:
            self.view.backgroundColor=[UIColor orangeColor];
            break;
        case 2:
            self.view.backgroundColor=[UIColor blueColor];
            break;
        case 3:
            self.view.backgroundColor=[UIColor magentaColor];
            break;
        case 4:
            self.view.backgroundColor=[UIColor greenColor];
            break;



            
        default:
            break;
    }
}


#pragma mark - View lifecycle

/*
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView
{
}
*/



- (void)viewDidLoad
{
    [super viewDidLoad];
    //
    markShown=NO;
    //this will help to identify the menu  if it has been already added to main view.
    selectedMenu.tag=SELECTED_MENU_ID;
    //
    circularContainer=[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"menuBg.png"]];
    circularContainer.frame=CGRectMake(0, 0, 75, 75);
    containerDefaultSize=circularContainer.frame.size;
    circularContainer.alpha=0;
    //
    deltaButton=[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"navDelta.png"]];
    deltaButton.alpha=0;
    //
    [self.view addSubview:circularContainer];
    [self.view addSubview:deltaButton];

    //make rounded corners for bound manager
    boundManager.layer.cornerRadius=boundManager.frame.size.width/2;
    [self initGestureRecognizers];
    //[self.view bringSubviewToFront:deltaButton];
    infoLabel.text=DEFAULT_HELP;
    //
    overlayView=[[UIView alloc] initWithFrame:boundManager.frame];
    overlayView.backgroundColor=[UIColor clearColor];
    overlayView.layer.cornerRadius=overlayView.frame.size.width/2;
    [self.view addSubview:overlayView];
    //bring the delta button to front
    [self.view bringSubviewToFront:deltaButton];
    //now create the menus
    [self createTheMenus];
    //the placeholder point,an arbitrary actually!
    placeHolder=CGPointMake(40, 420);
        
}


- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
