//
//  OrbController.h
//  doc2doc_pre
//
//  Created by ManGoes Mobile on 29/4/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TrackBall.h"

@interface OrbController : UIViewController
{
    TrackBall *trackBall;
}
@property (strong,nonatomic) TrackBall *trackBall;
@property (strong,nonatomic) IBOutlet UIView* transformed;
@end
