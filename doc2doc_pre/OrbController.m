//
//  OrbController.m
//  doc2doc_pre
//
//  Created by ManGoes Mobile on 29/4/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import "OrbController.h"

@implementation OrbController
@synthesize trackBall,transformed;

//touch based controlling
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    CGPoint location = [[touches anyObject] locationInView:self.view];
    //
    NSLog(@"Touched:x=%f,y=%f",location.x,location.y);
    //
    if(nil == self.trackBall) {
        self.trackBall = [TrackBall trackBallWithLocation:location inRect:self.view.bounds];
    } else {
        [self.trackBall setStartPointFromLocation:location];
    }
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
    CGPoint location = [[touches anyObject] locationInView:self.view];
    //
    NSLog(@"Touch Moved:x=%f,y=%f",location.x,location.y);
    //
    CATransform3D transform = [trackBall rotationTransformForLocation:location];
    transformed.layer.sublayerTransform = transform;
}


- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    CGPoint location = [[touches anyObject] locationInView:self.view];
    [self.trackBall finalizeTrackBallForLocation:location];
}




- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

/*
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView
{
}
*/


- (void)viewDidLoad
{
    [super viewDidLoad];
    transformed.userInteractionEnabled=NO;
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
