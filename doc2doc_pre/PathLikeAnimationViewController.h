//
//  PathLikeAnimationViewController.h
//  doc2doc_pre
//
//  Created by ManGoes Mobile on 20/4/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Animator.h"
#import "AwesomeMenu.h"
#define CIRCLE_INITIAL_DIAMETER 32.0
#define MENU_TAG 33242
#define CONTAINER_TAG 322343


@interface PathLikeAnimationViewController : UIViewController< AwesomeMenuDelegate>
{
    IBOutlet UIButton *addButton;
    BOOL buttonTapped;
    BOOL menuButtonShowed;
    Animator *animator;
    NSMutableArray *options;
    AwesomeMenu *_menu;
    UIPinchGestureRecognizer *pinchGesture;
    UIView *circularContainer;
    
}
@property(strong,nonatomic) IBOutlet UIView* container;
@property(strong,nonatomic) IBOutlet UILabel* label;
- (IBAction)addButtonPressed:(id)sender;

@end
