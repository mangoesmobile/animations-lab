//
//  DeltaNavController.m
//  doc2doc_pre
//
//  Created by ManGoes Mobile on 23/4/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import "DeltaNavController.h"

@implementation DeltaNavController
@synthesize deltaButton;
@synthesize startTransform;
@synthesize navLabel;
@synthesize last_controller;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
//-------------------------
// Return val is in radians
//-------------------------
-(float)getAngleBetweenPoint1:(CGPoint)point1 AndPoint2:(CGPoint)point2 
{
    float dx, dy,x1,y1,x2,y2, res;
    x1=point1.x;
    x2=point2.x;
    y1=point1.y;
    y2=point2.y;
    //
    dx = x1 - x2;
    dy = y1 - y2;
    
    if ( fabs(dx) < 0.0000001f && fabs(dy) < 0.0000001f ) {
        return 0;
    }
    
    if ( dx == 0 && y1 > y2 ) {   return 0;   }
    if ( dy == 0 && x1 < x2 ) {   return (0.5 * M_PI);  }
    if ( dx == 0 && y1 < y2 ) {   return M_PI; }
    if ( dy == 0 && x1 > x2 ) {   return (M_PI + (0.5 * M_PI)); }
    
    res = atanf( dy / dx );
    if ( res < 0 ) {
        res = (0.5 * M_PI) + res;
    }
    
    if ( x1 > x2  && y1 < y2 ) {   	// 2nd quadrant
        res += 0.5 * M_PI;
    } else if ( x1 > x2 && y1 > y2 ) { 		// 3rd quadrant
        res += M_PI;
    } else if ( x1 < x2 && y1 > y2 ) {		// 4th quadrant
        res += M_PI + (0.5 * M_PI);
    }
    return res;
}

-(CGFloat)DegreesToRadians:(CGFloat)degrees
{
    return degrees * M_PI / 180;
};

-(CGFloat)RadiansToDegrees:(CGFloat)radians
{
    return radians * 180 / M_PI;
};

-(int)posTrackedForAngle:(float)angle
{
    int posMark=-1;//means invalid
    if(angle>=165 && angle<190)
    {
        posMark=4;//right
    }
    if(angle>90 && angle<165)
    {
        posMark=3;//top-right
    }
    if(angle>10 && angle<80)
    {
        posMark=2;//top-left
    }
    if((angle>0 && angle<10) || (angle>340 && angle<360))
    {
        posMark=1;//left
    }
    return posMark;
}



//called when touches began on screen
-(void)touchesBegan:touches withEvent:event
{
    NSLog(@"Inside touches began");
    UITouch *touch=[[event allTouches] anyObject];
    CGPoint loc=[touch locationInView:self.view];
    if((loc.x>=deltaButton.frame.origin.x && loc.x<=deltaButton.frame.origin.x+deltaButton.frame.size.width) && (loc.y>=deltaButton.frame.origin.y && loc.y<=deltaButton.frame.origin.y+deltaButton.frame.size.height))
    {
        navStarted=YES;
        float dx=loc.x-pivot.x;
        float dy=loc.y-pivot.y;
        //
        startTransform=deltaButton.transform;
        //
        deltaButton.imageView.image=[UIImage imageNamed:@"praxis_glow.png"];
        
        deltaAngle = atan2(dy,dx); 
        
        NSLog(@"delta angle=%f",deltaAngle);
    }
    
}

- (void) touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
    
    UITouch *touch=[[event allTouches] anyObject];
    CGPoint loc=[touch locationInView:self.view];
    if(navStarted)
    {
        float dx=loc.x-pivot.x;
        float dy=loc.y-pivot.y;
        //float ang = atan2(dy,dx);
        float ang=[self getAngleBetweenPoint1:loc AndPoint2:pivot];
        float labelWidth1=label1.frame.size.width;
        float labelWidth2=label2.frame.size.width;
        float angleDifference = deltaAngle - ang;
        pos=[self posTrackedForAngle:[self RadiansToDegrees:ang]];
        switch (pos) {
            case 4:
                navLabel.text=@"Navigated Right.";
                if(labelWidth1<=100)
                {
                    label1.frame=CGRectMake( deltaButton.frame.origin.x+deltaButton.frame.size.width/2, deltaButton.frame.origin.y+4, labelWidth1+fixed_dx, deltaButton.frame.size.height-7);
                    if(labelWidth1==100)
                    {
                        [self performSegueWithIdentifier:@"segue4" sender:self];
                    }
                }
                break;
            case 3:
                navLabel.text=@"Navigated Top-Right.";
                
                break;
            case 2:
                navLabel.text=@"Navigated Top-left.";
                break;
            case 1:
                navLabel.text=@"Navigated Left.";
                
                //last_controller.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal; 
                break;

                
            default:
                break;
        }


        NSLog(@"dx=%f,dy=%f,angleDiff=%f,angle=%f",dx,dy,angleDifference,[self RadiansToDegrees:ang]);
    }

    
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    if(navStarted)
    {
        deltaButton.imageView.image=[UIImage imageNamed:@"praxis.png"];
        navStarted=NO;
        if(pos==4)//right nav was ended
        {
            pos=-1;
            [UIView beginAnimations:nil context:nil];
            [UIView setAnimationDuration:0.2];
            
            label1.frame=CGRectMake(deltaButton.frame.origin.x+deltaButton.frame.size.width/2, deltaButton.frame.origin.y+4, 0, deltaButton.frame.size.height-7);
            
            [UIView commitAnimations];

            
        }
        if(pos==3)//top right was ended
        {
            pos=-1;
            [UIView beginAnimations:nil context:nil];
            [UIView setAnimationDuration:0.5];
            
            label2.frame=CGRectMake(deltaButton.frame.origin.x+deltaButton.frame.size.width, deltaButton.frame.origin.y+4, 0, deltaButton.frame.size.height-7);
            
            [UIView commitAnimations];

        }
    }
}


- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

/*
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView
{
}
*/


- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.view setUserInteractionEnabled:YES];
    deltaButton.userInteractionEnabled=NO;
    navStarted=NO;
    pivot=deltaButton.center;
    deltaButton.layer.anchorPoint=CGPointMake(0.5f, 0.5f);
    fixed_dx=5.0;
    fixed_dy=5.0;
    //right_label
    label1=[[UILabel alloc] init];
    label1.frame=CGRectMake(deltaButton.frame.origin.x+deltaButton.frame.size.width/2, deltaButton.frame.origin.y+4, 0, deltaButton.frame.size.height-7);
    label1.backgroundColor=[UIColor orangeColor];
    label1.text=@"3d Orb Screen";
    label1.textAlignment=UITextAlignmentRight;
    /*
    //top-right label
    label2=[[UILabel alloc] init];
    label2.frame=CGRectMake(deltaButton.frame.origin.x+deltaButton.frame.size.width/2, deltaButton.frame.origin.y+4, WIDTH_OF_LABEL, deltaButton.frame.size.height-7);
    label2.backgroundColor=[UIColor orangeColor];
    label2.layer.anchorPoint=CGPointMake(1.0f, 0.0f);
    float rotationAngle=atan(label1.frame.size.height/(WIDTH_OF_LABEL-deltaButton.frame.size.width/2));
    NSLog(@"Rotation angle:%f",[self RadiansToDegrees:rotationAngle]);
    //
    label2.transform=CGAffineTransformMakeRotation(-rotationAngle);
    //
    label2.text=@"Reference";
    label2.textAlignment=UITextAlignmentRight;
     */
    [self.view addSubview:label1];
    //[self.view addSubview:label2];
    [self.view bringSubviewToFront:deltaButton];
    //save the previous one controller.
    last_controller=((doc2doc_preAppDelegate*)[UIApplication sharedApplication].delegate).current_controller;
    

}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
