//
//  DeltaNavController.h
//  doc2doc_pre
//
//  Created by ManGoes Mobile on 23/4/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#define WIDTH_OF_LABEL 100
#import "doc2doc_preAppDelegate.h"

@interface DeltaNavController : UIViewController
{
    IBOutlet UIButton* deltaButton;
    CGPoint pivot;
    BOOL navStarted;
    int pos;
    float deltaAngle;
    float fixed_dx;
    float fixed_dy;
    UILabel *label1;
    UILabel *label2;
    UIViewController *last_controller;
}
@property(strong,nonatomic) IBOutlet UIButton* deltaButton;
@property(strong,nonatomic) IBOutlet UILabel* navLabel;
@property CGAffineTransform startTransform;
@property(strong,nonatomic)UIViewController *last_controller;


@end
