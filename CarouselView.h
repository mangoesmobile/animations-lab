//
//  CarouselView.h
//  doc2doc_pre
//
//  Created by ManGoes Mobile on 29/4/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
@interface CarouselView : UIView
{
    int level;
    int direction;//-1 if no direction is available,1 for right,0 for left
}
@property int level;
@property int direction;
@end
